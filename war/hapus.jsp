<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="id">
    <head>
        <title>hapus</title>
    </head>
    <body>
        <h1>Hapus apa ga ?</h1>
        <form action="/hapus" method="post">
            <input type="hidden" name="hdnId" value="${data.key.id}">
            No HP: <input type="text" name="txtNoHP" value="${data.properties.noHP}" disabled><br/>
            Nama: <input type="text" name="txtNama" value="${data.properties.Nama}"><br/>
            NIM: <input type="text" name="txtNIM" value="${data.properties.NIM}"><br/>
            Email: <input type="text" name="txtEmail" value="${data.properties.Email}"><br/>
            Aktif: <input type="number" name="txtAktif" value="${data.properties.Aktif}"><br/>
            <input type="submit" value="Hapus">
        </form>
    </body>
</html>